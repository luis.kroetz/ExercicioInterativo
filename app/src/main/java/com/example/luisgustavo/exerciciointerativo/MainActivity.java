package com.example.luisgustavo.exerciciointerativo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    TextView textoMostrar;
    Button   btnMostrar;
    Button   btnFechar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textoMostrar = (TextView) findViewById(R.id.textoMostrar);
        btnMostrar = (Button) findViewById(R.id.btnMostrar);
        btnFechar = (Button) findViewById(R.id.btnFechar);
    }

    public void onClickMostrar (View aux) {
        textoMostrar.setVisibility(View.VISIBLE);
        btnMostrar.setVisibility(View. INVISIBLE);
        btnFechar.setVisibility(View. VISIBLE);
    }

    public void onClickFechar (View aux2) {
        textoMostrar.setVisibility(View.INVISIBLE);
        btnFechar.setVisibility(View. INVISIBLE);
        btnMostrar.setVisibility(View. VISIBLE);
    }

}
